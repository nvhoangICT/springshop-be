package com.example.shopapp.repository;

import com.example.shopapp.model.Product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import lombok.RequiredArgsConstructor;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    boolean existsByName(String name);
//    @NotNull Page<Product> findAll(@NotNull Pageable pageable);
}
