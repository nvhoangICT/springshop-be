package com.example.shopapp.repository;

import com.example.shopapp.model.Order;
import com.example.shopapp.model.Product;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
//    List<Order> findByUserId(Long userId);
}
