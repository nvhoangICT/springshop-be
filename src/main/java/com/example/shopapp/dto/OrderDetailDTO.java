package com.example.shopapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailDTO {
    @JsonProperty("order_id")
    @NotNull(message = "Order ID is required")
    private Long orderId;

    @JsonProperty("product_id")
    @NotNull(message = "Product ID is required")
    private Long productId;

    @PositiveOrZero(message = "Price must be a positive number")
    private Float price;

    @JsonProperty("number_of_products")
    @PositiveOrZero(message = "Number of products must be a positive or zero value")
    private int numberOfProducts;

    @JsonProperty("total_money")
    @PositiveOrZero(message = "Total money must be a positive number")
    private Float totalMoney;

    private String color;
}