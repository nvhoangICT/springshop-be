package com.example.shopapp.service;

import com.example.shopapp.dto.OrderDTO;
import com.example.shopapp.exception.DataNotFoundException;
import com.example.shopapp.model.Order;
import com.example.shopapp.response.OrderResponse;

import java.util.List;

public interface IOrderService {

    Order createOrder(OrderDTO order) throws DataNotFoundException;

    Order getOrderById(long id);

    List<Order> getAllOrders();

    Order updateOrder(long orderId, OrderDTO order);

    void deleteOrder(long id);
}
