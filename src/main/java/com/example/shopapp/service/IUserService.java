package com.example.shopapp.service;

import com.example.shopapp.dto.UserDTO;
import com.example.shopapp.exception.DataNotFoundException;
import com.example.shopapp.model.User;

import org.springframework.stereotype.Service;

public interface IUserService {

    User createUser(UserDTO userDTO) throws DataNotFoundException;

    String loginUser(String phoneNumber, String password);
}
